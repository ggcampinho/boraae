class FacebookController < ApplicationController

	def channel
		response.headers["Pragma"] = "public"
		response.headers["Cache-Control"] = "max-age=#{1.year.to_i}"
		response.headers["Expires"] = 1.year.from_now.to_s(:rfc822)
	end

end
