# -*- encoding : utf-8 -*-
class UserSessionsController < ApplicationController
  before_filter :load_access_token, :only => [ :create ]
	layout "basic"
  
  # GET /user_sessions/new
  # GET /user_sessions/new.json
  def new
    @user_session = UserSession.new
		
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @user_session }
    end
  end

  # POST /user_sessions
  # POST /user_sessions.json
  def create
    user = User.find_or_create_from_facebook params[:user]
    @user_session = UserSession.new user
    login
  end

  # DELETE /user_sessions/1
  # DELETE /user_sessions/1.json
  def destroy
    @user_session = UserSession.find(params[:id])
    @user_session.destroy unless @user_session.blank?

    respond_to do |format|
      format.html { redirect_to root_url }
      format.json { head :no_content }
    end
  end

  def request_password
  end
  
  protected
  
  def login
    if @user_session.save
      user = @user_session.record 
      user.facebook_token = Facebook.access_token
      user.save!
      user.update_all_from_facebook
      @redirect_url = calendar_events_url
      respond_to do |format|
        format.html { redirect_to @redirect_url }
        format.js
        format.json { render :json => @user_session, :status => :created, :location => @user_session }
      end
    else
      respond_to do |format|
        format.html { redirect_to root_url, :notice => 'Invalid login' }
        format.json { render :json => @user_session.errors, :status => :unprocessable_entity }
      end
    end
  end
  
end
