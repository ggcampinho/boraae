# -*- encoding : utf-8 -*-
class EventsController < ApplicationController
  
  def calendar
    index 'after'
  end
  
  def history
    index 'before'
  end
  
  def index(method = 'after')
    @last_time = Time.at(params[:last_time].to_i + 1) unless params[:last_time].blank?
    @last_time ||= Time.now - 12.hours
    
      events = Event.send method, @last_time, 50, current_user
      @last_time = events.last.start_time unless events.blank?
  
      @events_per_day = {}
      events.each do |event|
              @events_per_day[event.start_time.beginning_of_day] ||= []
              @events_per_day[event.start_time.beginning_of_day] << event
      end
    
    @has_more_events = events.blank? ? false : Event.send("has_more_#{method}?", @last_time, current_user)
    @last_time = @last_time.to_i unless events.blank?
    
    respond_to do |format|
      format.html { render :action => 'index' }
      format.js { render :action => 'index' }
    end
  end
  
end
