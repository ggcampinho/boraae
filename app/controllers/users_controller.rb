# -*- encoding : utf-8 -*-
class UsersController < ApplicationController
  before_filter :verify_access_token, :only => [ :new ]
  
  # GET /users
  # GET /users.json
  def index
    @users = User.limit(10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
  		hash = User.attributes_from_facebook Facebook.me
			@user = User.new hash
      @picture_url = Facebook.user_picture(@user.facebook_id)
		
			respond_to do |format|
				format.html # new.html.erb
				format.json { render :json => @user }
			end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
		@user = User.new params[:user]
    @user.picture_from_url params[:picture_url]
    
		if @user.save_without_session_maintenance
      return_url = url_for :controller => 'user_sessions', :action => 'create_from_facebook', :only_path => false
    else
      return_url = url_for :controller => 'users', :action => 'new', :only_path => false
    end
    authorize_url = Facebook.authorize_url :return_url => return_url, :state => create_facebook_state
    
    respond_to do |format|
    	#format.json { render :json => @user, :status => :created, :location => @user }
      #format.json { render :json => @user.errors, :status => :unprocessable_entity }
      format.html { redirect_to authorize_url }
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, :notice => 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => nil, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
end
