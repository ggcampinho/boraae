# -*- encoding : utf-8 -*-
module ApplicationHelper
	def pluralize_text(number, singular, plural)
		pluralize(number, singular, plural).split.last
	end
	
	def print_hour(time)
		l time, :format => (time.min == 0 ? :hours : :hours_and_minutes)
	end
end
