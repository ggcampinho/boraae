# -*- encoding : utf-8 -*-
module EventsHelper
	def duration(start_time, end_time)
		duration = "#{print_hour start_time} até #{print_hour end_time}"
		if start_time.beginning_of_day != end_time.beginning_of_day
			duration += " de #{l end_time, :format => :day_and_long_month}"
		end
		duration
	end
end
