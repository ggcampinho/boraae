# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

window.facebookInit = () ->
	FB.init {
		appId: '152484818179945', 				# App ID
		channelUrl: '//bora.ae/channel', 	# Channel File
		status: true, 										# check login status
		cookie: true, 										# enable cookies to allow the server to access the session
		xfbml: true	  										# parse XFBML
	}
  
	login = (response) ->
		if response.authResponse
			# user has auth'd your app and is logged into Facebook
			FB.api '/me', (me) ->
				jqXHR = $.ajax {
					url: '/user_sessions',
					data: { user: me, auth_response: response.authResponse },
					type: 'POST',
					dataType: 'script'
				}
				jqXHR.fail () ->
					$('.btn.facebook-login').trigger 'cancel'
		else
			$('.btn.facebook-login').trigger 'cancel'
  
	$('.btn.facebook-login').on 'loading', (e) ->
		$(this).data 'html', $(this).html()
		$(this).text 'Carregando...'
  
	$('.btn.facebook-login').on 'cancel', (e) ->
		$(this).html $(this).data('html')
		alert 'Erro ao conectar com o facebook, por favor tente novamente ou recarregue a página.'
  
	$('.btn.facebook-login').on 'click', (e) ->
		$(this).trigger 'loading'
		FB.getLoginStatus (response) ->
			if response.status == 'connected'
					login response
			else
				# respond to clicks on the login and logout links
				FB.login login, { scope: 'email,user_birthday,user_location,user_events,friends_birthday,friends_events,friends_location,publish_stream,create_event,rsvp_event' }
		e.preventDefault()

if window.FB
	window.facebookInit()
else
	window.fbAsyncInit = window.facebookInit
