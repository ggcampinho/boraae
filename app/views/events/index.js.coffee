events = $('.events-per-day')
events.children('li:last').remove()

newEvents = $(<%= render :partial => 'events', :object => @events, :locals => { :has_more_events => @has_more_events, :last_time => @last_time } %>)
newEvents.find('.day-events').each (i, dayEvents) ->
	oldDayEvents = events.find '#' + $(dayEvents).attr('id')
	if oldDayEvents.length > 0
		$(dayEvents).find('.event').each (j, event) ->
			oldEvent = oldDayEvents.find '#' + $(event).attr('id')
			if oldEvent.length == 0
				oldDayEvents.append $(event)
	else
		events.append $(dayEvents).parents('li:first')

events.append newEvents.filter(':last')
