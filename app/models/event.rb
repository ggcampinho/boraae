# -*- encoding : utf-8 -*-

require 'open-uri'

class Event < ActiveRecord::Base
	
	has_attached_file :picture,
		:styles => { :original => '100%' },
		:storage => :s3,
		:whiny => false,
		:s3_credentials => "#{Rails.root}/config/s3.yml",
		:path => "/event/:style/:id.:extension"
	
	validates_uniqueness_of :facebook_id, :allow_nil => true
	
	belongs_to :user
	belongs_to :location
	has_many :event_users
	has_many :users, :through => :event_users
	
	attr_accessor :friends_invitee
	
	def self.find_or_create_from_facebook(facebook_event, options = {})
		Event.find_or_create_by_facebook_id Event.attributes_from_facebook(facebook_event), options
	end
	
	def update_from_facebook(facebook_event, options = {})
		update_attributes_from_facebook facebook_event, options
		update_picture_from_facebook options
	end
	
	def update_attributes_from_facebook(facebook_event, options = {})
		hash = Event.attributes_from_facebook facebook_event
		if facebook_event.has_key? 'owner'
			self.user = User.find_or_create_from_facebook facebook_event['owner'], :validate => false
		end
		if facebook_event.has_key? 'location'
 			self.location = Location.create_or_update_from_facebook facebook_event['location'], facebook_event['venue']
 		end
		update_attributes! hash, options
		self
	end
	
	def update_picture_from_facebook(options = {})
		self.picture = File.load_from_url Facebook.event_picture(facebook_id)
		save! options
		self
	end
	
	def update_facebook_data(user, async = true)
		if async
			Delayed::Job.enqueue EventJob::UpdateFacebookData.new(self, user)
		else
			puts "Updating event #{id}"
			Facebook.access_token_from_user user
			update_from_facebook Facebook.object(facebook_id)
			puts "Event #{id} updated"
		end
	rescue OAuth2::Error => exception
		raise exception unless exception.type == Facebook::Error::OAuthException and exception.code == 100
		puts "Invalid access token from user #{user.id}"
	end
	
	def update_facebook_invited(token_user, async = true)
		if async
			Delayed::Job.enqueue EventJob::UpdateFacebookInvited.new(self, user)
		else
			puts "Updating event #{id} invited"
			begin
				Facebook.access_token_from_user token_user
				hash = Facebook.event_invited facebook_id
				while hash['data'].length > 0
					hash['data'].each do |h|
						event_user = EventUser.find_or_update_from_facebook self, h
						unless event_users.exists?( :user_id => event_user.user.id, :event_id => event_user.event.id)
							event_users << event_user
						end
						puts "Event_user #{event_user.id} updated"
					end
					hash = Facebook.event_invited id, hash['paging']['next'].params
				end
			rescue OAuth2::Error => exception
				raise exception unless exception.type == Facebook::Error::OAuthException and exception.code == 100
				puts "Invalid access token from user #{token_user.id}"
			end
			puts "Event #{id} invited updated"
		end
	end
	
	def self.before(start_time, limit, user)
		in_calendar start_time, '<=', limit, 'DESC', user
	end
	
	def self.after(start_time, limit, user)
		in_calendar start_time, '>=', limit, 'ASC', user
	end
	
	def self.has_more_after?(start_time, user)
		has_more_in_calendar? start_time, '>', user
	end
	
	def self.has_more_before?(start_time, user)
		has_more_in_calendar? start_time, '<', user
	end
	
	def self.attributes_from_facebook(facebook_event)
		hash = {}
		hash['facebook_id'] = facebook_event['id']
		
		keys = %w(start_time end_time)
		keys.each do |key|
			hash[key] = Time.parse facebook_event[key]
		end
		
		keys = %w(name description privacy)
		keys.each do |key|
			hash[key] = facebook_event[key]
		end
		hash
	end
	
	protected
	
	def self.get_attributes_to_update(facebook_event)
		attributes = []
		[:start_time, :end_time, :name, :description, :privacy, :user, :location].each do |key|
			attributes << key if facebook_event.has_key? key.to_s
		end
		attributes
	end
	
	def self.in_calendar(time, operator, limit, order, user)
		events = joins(:event_users).where("events.start_time #{operator} :start_time AND event_users.user_id IN (:users, :user)", { :start_time => time, :users => user.friend_ids, :user => user.id }).order("events.start_time #{order}").group('events.id').limit(limit)
		events |= joins(:event_users).where('events.start_time = :start_time AND event_users.user_id IN (:users, :user)', { :start_time => events.last.start_time, :users => user.friend_ids, :user => user.id }).order("events.start_time #{order}").group('events.id').limit(limit) unless events.blank?
		events.each do |event|
			event.friends_invitee = EventUser.where('event_users.event_id = :event AND event_users.user_id IN (:users, :user) AND rsvp_status IN (:rsvp_status)', { :event => event.id, :users => user.friend_ids, :user => user.id, :rsvp_status => [ 'attending', 'unsure' ] }).order('event_users.rsvp_status ASC')
		end
		events.keep_if do |event|
			(!event.privacy.blank? and event.privacy != 'SECRET') or event.friends_invitee.where('user_id = :user_id', { :user_id => user.id }).count > 0
		end
		events
	end
	
	def self.has_more_in_calendar?(time, operator, user)
		joins(:event_users).where("events.start_time #{operator} :start_time AND ((events.privacy IN (:privacy) AND event_users.user_id IN (:users, :user)) OR (events.privacy IS NULL AND event_users.user_id = :user))", { :start_time => time, :privacy => [ 'OPEN', 'CLOSED' ], :users => user.friend_ids, :user => user.id }).group('events.id').all.count > 0
	end
end
