# -*- encoding : utf-8 -*-
class Location < ActiveRecord::Base
	
	def self.create_or_update_from_facebook(location_name, facebook_location)
		location = Location.new attributes_from_facebook(location_name, facebook_location)
		Location.transaction do
			stored_location = Location.where('facebook_id = :facebook_id OR name = :name OR (latitude = :latitude AND longitude = :longitude)', { :facebook_id => location.facebook_id, :name => location.name, :latitude => location.latitude, :longitude => location.longitude }).first
			unless stored_location.blank?
				Location.get_attributes_to_update(facebook_location).each do |attr|
					stored_location.send attr.to_s + '=', location.send(attr.to_s)
				end
				location = stored_location
			end
			raise Exception.new(location.errors.pretty_inspect) unless location.save
		end
		location
	end
	
	def self.attributes_from_facebook(location_name, facebook_location)
		hash = {}
		facebook_location ||= {}
		hash['facebook_id'] = facebook_location['id']
		hash['name'] = location_name
		
		keys = %w(street city state zip country latitude longitude)
		keys.each do |key|
			hash[key] = facebook_location[key]
		end
		
		success = true
		hash.each do |key, value|
			success = true unless hash[key].blank?
		end
		success ? hash : nil
	end
	
	protected
	
	def self.get_attributes_to_update(facebook_event)
		[:name, :street, :city, :state, :zip, :country, :latitude, :longitude]
	end
end
