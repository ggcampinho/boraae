# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
	acts_as_authentic do |config|
		config.require_password_confirmation = false
		config.ignore_blank_passwords = true
  end
	
	attr_accessible :facebook_id, :email, :name, :password, :password_confirmation, :birthday, :gender, :locale, :timezone
	
	has_attached_file :picture,
		:styles => { :original => '100%', :square => '200x200#', :medium_square => '100x100#', :small_square => '50x50#' },
		:storage => :s3,
		:whiny => false,
		:s3_credentials => "#{Rails.root}/config/s3.yml",
		:path => "/user/:style/:id.:extension"
	
	validates_uniqueness_of :facebook_id
	
	has_many :friendships
	has_many :friends, :through => :friendships
	has_many :event_users
	has_many :events, :as => :invited_events, :through => :event_user
	has_many :events, :as => :invited_events, :through => :event_user
	
	before_create :reset_persistence_token
   
	def require_password?
		false
	end
	
	def self.find_or_create_from_facebook(facebook_user, options = {})
		user = User.find_or_create_by_facebook_id User.attributes_from_facebook(facebook_user), options
		if user.new_record?
			user.update_picture_from_facebook options
		end
		user
	end
	
	def update_from_facebook(facebook_user, options = {})
		update_attributes_from_facebook facebook_user, options
		update_picture_from_facebook options
	end
	
	def update_attributes_from_facebook(facebook_user, options = {})
		hash = User.attributes_from_facebook facebook_user
		update_attributes! hash, options
		self
	end
	
	def update_picture_from_facebook(options = {})
		self.picture = File.load_from_url Facebook.user_picture(facebook_id)
		save! options
		self
	end
	
	def update_all_from_facebook(token_user = self, async = true)
		update_facebook_data token_user, async
		update_facebook_events token_user, async
		update_facebook_friends token_user, async
	end
	
	def update_facebook_data(token_user = self, async = true)
		if async
			Delayed::Job.enqueue UserJob::UpdateFacebookData.new(self, token_user)
		else
			puts "Updating user #{id}"
			Facebook.access_token_from_user token_user
			update_from_facebook Facebook.user(facebook_id)
			puts "User #{id} updated"
		end
	rescue OAuth2::Error => exception
		raise exception unless exception.type == Facebook::Error::OAuthException and exception.code == 100
		puts "Invalid access token from user #{token_user.id}"
	end
	
	def update_facebook_events(token_user, async = true)
		if async
			Delayed::Job.enqueue UserJob::UpdateFacebookEvents.new(self, token_user)
		else
			puts "Updating user #{id} events"
			begin
				Facebook.access_token_from_user token_user
				hash = Facebook.user_events(facebook_id)
				while hash['data'].length > 0
					hash['data'].each do |h|
						event = Event.find_or_create_from_facebook h
						EventUser.find_or_create_from_facebook event, self, h
						event.update_facebook_data token_user
						puts "Event #{event.id}, friend #{self.id} updated"
					end
					hash = Facebook.user_events self.facebook_id, hash['paging']['next'].params
				end
			rescue OAuth2::Error => exception
				raise exception unless exception.type == Facebook::Error::OAuthException and exception.code == 100
				puts "Invalid access token from user #{token_user.id}"
			end
			puts "User #{id} events updated"
		end
	end
	
	def update_facebook_friends(token_user, async = true)
		if async
			Delayed::Job.enqueue UserJob::UpdateFacebookFriends.new(self, token_user)
		else
			begin
				puts "Updating user #{id} friends"
				Facebook.access_token_from_user token_user
				hash = Facebook.my_friends
		
				while hash['data'].length > 0
					hash['data'].each do |h|
						friend = User.find_or_create_from_facebook h, :validate => false
						friend.update_facebook_events token_user
						friends << friend unless friends.exists? friend
						puts "User #{friend.id} is a User #{id} friend"
					end
					hash = Facebook.my_friends hash['paging']['next'].params
				end
			rescue OAuth2::Error => exception
				raise exception unless exception.type == Facebook::Error::OAuthException and exception.code == 100
				puts "Invalid access token from user #{token_user.id}"
			end
			puts "User #{id} friends updated"
		end
	end
	
	def facebook_token=(token)
		self[:facebook_token] = token.blank? ? nil : token.token
		self[:facebook_token_expires_at] = token.blank? ? nil : Time.at(token.expires_at)
	end
	
	def self.attributes_from_facebook(facebook_user)
		hash = {}
		hash['facebook_id'] = facebook_user['id']
		hash['birthday'] = Time.parse facebook_user['birthday']
		
		keys = %w(email gender locale timezone name)
		keys.each do |key|
			hash[key] = facebook_user[key]
		end
		hash
	end
	
end
