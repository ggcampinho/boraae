# -*- encoding : utf-8 -*-
class EventUser < ActiveRecord::Base
	belongs_to :event
	belongs_to :user
	
	def self.find_or_create_from_facebook(event, user, facebook_event_user, options = {})
		hash = EventUser.attributes_from_facebook event, user, facebook_event_user
		event_user = EventUser.find_or_create_by_event_id_and_user_id hash, options
	end
	
	def update_from_facebook(facebook_event_user, options = {})
		update_attributes_from_facebook facebook_event_user, options
	end
	
	def update_attributes_from_facebook(facebook_event_user, options = {})
		hash = EventUser.attributes_from_facebook facebook_event_user
		update_attributes! hash, options
		self
	end
	
	def self.attributes_from_facebook(event, user, facebook_user_event)
		hash = {}
		keys = %w(rsvp_status)
		keys.each do |key|
			hash[key] = facebook_user_event[key]
		end
		hash[:user] = user
		hash[:user_id] = user.id
		hash[:event] = event
		hash[:event_id] = event.id
		hash
	end
	
end
