# -*- encoding : utf-8 -*-
class Facebook
	@@access_token = nil
	
	def self.access_token
		@@access_token
	end
	
	def self.access_token=(token)
		@@access_token = token
	end
	
	def self.access_token_from_auth_response(auth_response)
		@@access_token = OAuth2::AccessToken.new client, auth_response['accessToken'], { :expires_in => auth_response['expiresIn'].to_i - 60, :param_name => "access_token", :mode => :query }
	end
	
	def self.access_token_from_user(user)
		@@access_token = OAuth2::AccessToken.new client, user.facebook_token, { :expires_at => user.facebook_token_expires_at.to_i, :param_name => "access_token", :mode => :query }
	end

	def self.client
  	OAuth2::Client.new Facebook::App::ID, Facebook::App::SECRET, :site => 'https://graph.facebook.com', :token_url => '/oauth/access_token'
	end
	
	def self.authorize_url(options)
		"https://www.facebook.com/dialog/oauth?client_id=#{Facebook::App::ID}&redirect_uri=#{CGI.escape options[:return_url]}&state=#{options[:state]}&scope=email,user_birthday,user_location,user_events,friends_birthday,friends_events,friends_location,publish_stream,create_event,rsvp_event"
	end

	def self.get_access_token(code, return_url)
		short_term_token = client.auth_code.get_kv_token code, :redirect_uri => return_url
		@@access_token = client.auth_code.get_long_term_kv_token short_term_token.token
		@@access_token.options[:param_name] = "access_token"
		@@access_token.options[:mode] = :query
		@@access_token
	end
	
	def self.object(id)
		@@access_token.get(id).parsed
	end
	
	def self.user_picture(id)
		hash = @@access_token.get('fql', :params => { :q => "SELECT pic_big FROM user WHERE uid=#{id}"}).parsed['data']
		hash.length > 0 ? hash[0]['pic_big'] : nil
	end
	
	def self.event_picture(id)
		hash = @@access_token.get('fql', :params => { :q => "SELECT pic_big FROM event WHERE eid=#{id}"}).parsed['data']
		hash.length > 0 ? hash[0]['pic_big'] : nil
	end
	
	def self.user(id, params = {})
		@@access_token.get(id, :params => params).parsed
	end
	
	def self.me
		@@access_token.get('me').parsed
	end
	
	def self.my_friends(params = {})
		@@access_token.get('me/friends', :params => params).parsed
	end
	
	def self.my_events(params = {})
		@@access_token.get('me/events', :params => params).parsed
	end
	
	def self.event_invited(id, params = {})
		@@access_token.get("#{id}/invited", :params => params).parsed
	end
	
	def self.user_events(id, params = {})
		@@access_token.get("#{id}/events", :params => params).parsed
	end
	
end	
