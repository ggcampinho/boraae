# -*- encoding : utf-8 -*-
class AddFacebookTokenToUsers < ActiveRecord::Migration
  def up
  	change_table :users do |t|
  		t.string :facebook_token
  		t.datetime :facebook_token_expires_at
  	end
  end
  
  def down
  	remove_column :users, :facebook_token
  	remove_column :users, :facebook_token_expires_at
  end
end
