class IndexFacebookIdForUser < ActiveRecord::Migration
  def self.down
    change_table :users do |t|
      t.index :facebook_id
    end
  end

  def self.down
    remove_index :users, :facebook_id
  end
end
