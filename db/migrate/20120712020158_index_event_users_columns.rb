class IndexEventUsersColumns < ActiveRecord::Migration
  def self.up
    change_table :event_users do |t|
      t.index :rsvp_status
      t.index :event_id
      t.index :user_id
    end
  end

  def self.down
    remove_index :event_users, :rsvp_status
    remove_index :event_users, :event_id
    remove_index :event_users, :user_id
  end
end
