# -*- encoding : utf-8 -*-
class AddUpdatedAtColumnsToUser < ActiveRecord::Migration
  def up
  	change_table :users do |t|
  		t.datetime :updated_facebook_data_at
  		t.datetime :updated_facebook_events_at
      t.datetime :updated_facebook_friends_at
  	end
  end
  
  def down
    remove_column :users, :updated_facebook_data_at
  	remove_column :users, :updated_facebook_events_at
  	remove_column :users, :updated_facebook_friends_at
  end
end
