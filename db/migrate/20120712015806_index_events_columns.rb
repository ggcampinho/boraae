class IndexEventsColumns < ActiveRecord::Migration
  def self.up
    change_table :events do |t|
      t.index :facebook_id
      t.index :start_time
      t.index :privacy
    end
  end

  def self.down
    remove_index :events, :facebook_id
    remove_index :events, :start_time
    remove_index :events, :privacy
  end
end
