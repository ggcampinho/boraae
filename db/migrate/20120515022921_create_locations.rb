# -*- encoding : utf-8 -*-
class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.string :street
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.decimal :latitude
      t.decimal :longitude
			t.string :facebook_id
			
      t.timestamps
    end
  end
end
