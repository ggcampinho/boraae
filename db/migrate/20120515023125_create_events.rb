# -*- encoding : utf-8 -*-
class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :user_id
      t.string :name
      t.text :description
      t.datetime :start_time
      t.datetime :end_time
      t.integer :location_id
      t.string :privacy
      t.string :facebook_id

      t.timestamps
    end
  end
end
