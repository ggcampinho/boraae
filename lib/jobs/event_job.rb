# -*- encoding : utf-8 -*-
class EventJob

	class UpdateFacebookData
		def initialize(event, user)
			@event = event
			@user = user
		end
	
		def perform
		  @event.update_facebook_data @user, false
		end
	end
	
	class UpdateFacebookInvited
		def initialize(event, user)
			@event = event
			@user = user
		end
	
		def perform
		  @event.update_facebook_invited @user, false
		end
	end
	
end
