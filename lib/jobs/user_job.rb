# -*- encoding : utf-8 -*-
class UserJob

  class UpdateFacebookData
    def initialize(user, token_user)
      @user = user
      @token_user = token_user
    end

    def perform
      @user.update_facebook_data @token_user, false
    end
  end
  
  class UpdateFacebookEvents
    def initialize(user, token_user)
      @user = user
      @token_user = token_user
    end

    def perform
      @user.update_facebook_events @token_user, false
    end
  end
  
  class UpdateFacebookFriends
    def initialize(user, token_user)
      @user = user
      @token_user = token_user
    end

    def perform
      @user.update_facebook_friends @token_user, false
    end
  end
  
end
