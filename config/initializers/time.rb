# -*- encoding : utf-8 -*-
class Time
	def self.parse(string)
		time = nil
		if string =~ /^\d{2}\/\d{2}\/\d{4}$/
			time = Time.utc string[6..9], string[0..1], string[3..4]
		elsif string =~ /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/
			time = Time.utc string[0..3], string[5..6], string[8..9], string[11..12], string[14..15], string[17..18]
		end
		time
	end
end
