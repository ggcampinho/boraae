# -*- encoding : utf-8 -*-

require 'open-uri'

class File
	def self.load_from_url(url)
		f = nil
		unless url.blank?
			str = URI.parse(url).read
			f = File.new "#{Rails.root}/tmp/#{rand 99999}-#{Time.now.to_i}-#{url[/([^\/]{0,8})$/]}", "wb"
			f.write str
			f.close
		end
		f
	end
end
