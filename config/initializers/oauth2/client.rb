# -*- encoding : utf-8 -*-
module OAuth2
	class Client
  		def token_request(params)
  			response = nil
  			if self.options[:token_method] == :post
					response = request :post, token_url, :body => params, :raise_errors => true
				else
					response = request :get, token_url, :params => params, :raise_errors => true
				end
				response
  		end
	end
end
