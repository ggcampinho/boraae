# -*- encoding : utf-8 -*-
module OAuth2
	module Strategy
		class AuthCode < Base
			def token_params(code, params = {})
				{ 'grant_type' => 'authorization_code', 'code' => code }.merge(client_params).merge(params)
	  	end
	  	
	  	def long_term_token_params(short_term_token, params = {})
	  		{ 'grant_type' => 'fb_exchange_token', 'fb_exchange_token' => short_term_token }.merge(client_params).merge(params)
	  	end
	  	
			def get_kv_token(code, params = {})
				params = token_params(code, params)
				OAuth2::AccessToken.from_kvform_and_token_params(@client, params)
			end
			
			def get_long_term_kv_token(short_term_token, params = {})
				params = long_term_token_params(short_term_token, params)
				OAuth2::AccessToken.from_kvform_and_token_params(@client, params)
			end
		end
	end
end
