# -*- encoding : utf-8 -*-
module OAuth2
	class Error
		attr_accessor :type
		
		def initialize(response)
			response.error = self
			@response = response

			message = []
			if response.parsed.is_a? Hash
				parsed = response.parsed
				if parsed['error'].is_a? Hash
					parsed['error'].each do |key, value|
						message << "#{key}: #{value}"
					end
					@description = parsed['error']['message']
					@code = parsed['error']['code']
					@type = parsed['error']['type']
				else
					message << parsed['error']
					@description = parsed['error']
					@code = parsed['error_description']
				end
			else
				message << response.body
			end

			super message.join("\n")
		end
	end
end
