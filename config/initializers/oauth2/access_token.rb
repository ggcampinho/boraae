# -*- encoding : utf-8 -*-
module OAuth2
	class AccessToken
		def self.from_kvform_and_token_params(client, params={})
			response = client.token_request(params)
			from_kvform client, response.body
		end
	end
end
