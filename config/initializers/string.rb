# -*- encoding : utf-8 -*-
class String
	def params
		s = self[/\?.*/]
		s.blank? ? {} : Hash[s.scan(/[^?&]+/).map {|param| param.split '='}]
	end
end
