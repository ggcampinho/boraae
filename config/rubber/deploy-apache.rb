
namespace :rubber do

  namespace :apache do
  
    rubber.allow_optional_tasks(self)

    after "rubber:install_packages", "rubber:apache:install"

    task :install, :roles => :apache do
      rubber.sudo_script 'install_apache', <<-ENDSCRIPT
        a2dissite default

        # TODO: remove this once 12.04 is fixed
        # https://bugs.launchpad.net/ubuntu/+source/mod-proxy-html/+bug/964397
        if [[ ! -f /usr/lib/libxml2.so.2 ]]; then
          ln -sf /usr/lib/x86_64-linux-gnu/libxml2.so.2 /usr/lib/libxml2.so.2
        fi
      ENDSCRIPT
    end

    # serial_task can only be called after roles defined - not normally a problem, but
    # rubber auto-roles don't get defined till after all tasks are defined
    on :load do
      #rubber.serial_task self, :serial_restart, :roles => :apache do
      #  rsudo "service apache2 stop; service apache2 start"
      #end
      #rubber.serial_task self, :serial_reload, :roles => :apache do
      #  rsudo "if ! ps ax | grep -v grep | grep -c apache2 &> /dev/null; then service apache2 start; else service apache2 reload; fi"
      #end
      rubber.serial_task self, :serial_restart, :roles => [:app, :apache] do
        rsudo "service apache2 restart"
      end
      rubber.serial_task self, :serial_reload, :roles => [:app, :apache] do
        # remove file checked by haproxy to take server out of pool, wait some
        # secs for haproxy to realize it
        maybe_sleep = " && sleep 5" if RUBBER_ENV == 'production'
        rsudo "rm -f #{previous_release}/public/httpchk.txt #{current_release}/public/httpchk.txt#{maybe_sleep}"

        rsudo "if ! ps ax | grep -v grep | grep -c apache2 &> /dev/null; then service apache2 start; else service apache2 reload; fi"

        # Wait for passenger to startup before adding host back into haproxy pool
        logger.info "Waiting for passenger to startup"

        opts = get_host_options('rolling_restart_port') {|port| port.to_s}
        rsudo "while ! curl -s -f http://localhost:$CAPISTRANO:VAR$/ &> /dev/null; do echo .; done", opts

        # Touch the file so that haproxy adds this server back into the pool.
        rsudo "touch #{current_path}/public/httpchk.txt#{maybe_sleep}"
      end
    end
    
    before "deploy:stop", "rubber:apache:stop"
    after "deploy:start", "rubber:apache:start"
    after "deploy:restart", "rubber:apache:reload"
    
    desc "Stops the apache web server"
    task :stop, :roles => :apache do
      rsudo "service apache2 stop || true"
    end
    
    desc "Starts the apache web server"
    task :start, :roles => :apache do
      rsudo "service apache2 start"
      opts = get_host_options('rolling_restart_port') {|port| port.to_s}
      rsudo "while ! curl -s -f http://localhost:$CAPISTRANO:VAR$/ &> /dev/null; do echo .; done", opts
      rsudo "touch #{current_path}/public/httpchk.txt"
    end
    
    desc "Restarts the apache web server"
    task :restart, :roles => :apache do
      serial_restart
    end
  
    desc "Reloads the apache web server"
    task :reload, :roles => :apache do
      serial_reload
    end
  
  end


end
